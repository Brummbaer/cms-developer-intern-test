<?php

/**
 * Implements a command to import events from a JSON file.
 */
class Import_Events_Command {
	/**
	 * Imports events from a JSON file.
	 *
	 * ## OPTIONS
	 *
	 * <JSON_file>
	 * : The file to import events from.
	 *
	 * [--status=<status>]
	 * : The event's status in WordPress.
	 * ---
	 * default: publish
	 * options:
	 *   - draft
	 *   - publish
	 *   - pending
	 *   - private
	 * ---
	 *
	 *
	 * ## EXAMPLES
	 *
	 *     wp import events.json
	 *     wp import --status=draft events.json
	 *
	 * @when after_wp_load
	 */
	function __invoke( $args, $assoc_args ) {
		if ( count( $args ) < 1 ) {
			WP_CLI::error( "Missing JSON file." );
		}

		// Open the JSON file:
		$file = file_get_contents( $args[0] );

		$post_status = $assoc_args['status'];

		// Check for errors:
		if ( ! $file ) {
			WP_CLI::error( sprintf( "File %s doesn't exist or is unreadable", $args[0] ) );
		}

		$array = json_decode( $file, true );

		$event_inserted = 0;
		// Main loop that inserts events:
		foreach ( $array as $event ) {
			// Check that the timestamp is valid:
			$datetime = DateTime::createFromFormat( 'Y-m-d H:i:s', $event["timestamp"] );

			if ( ! $datetime ) {
				// Emit a warning if the timestamp is incorrect:
				WP_CLI::warning( sprintf( "Event[%d] contains invalid timestamp: %s", $event["id"],
					$event["timestamp"] ) );
				continue;
			}

			$event_args = array(
				'post_type'   => 'event',
				'post_title'  => $event["title"],
				'post_status' => $post_status,

				'meta_input' => array(
					'title'     => $event["title"],
					'about'     => $event["about"],
					'organizer' => $event["organizer"],
					'timestamp' => $event["timestamp"],
					'email'     => $event["email"],
					'address'   => $event["address"],
					'latitude'  => $event["latitude"],
					'longitude' => $event["longitude"],
				),
			);

			/* Check if the event doesn't already exist:
				if it does:     it's updated.
				If it doesn't:  it's inserted.
			*/
			if ( get_post_status( $event["id"] ) ) {
				$event_args += array( 'ID' => $event["id"] );
			} else {
				$event_args += array( 'import_id' => $event["id"] );
			}

			// Insert or update the event:
			$post_id = wp_insert_post( $event_args );

			// Insert/update the post's tags:
			wp_set_post_tags( $post_id, $event["tags"], false );

			if ( $post_id ) {
				$event_inserted ++;
				WP_CLI::success( sprintf( "Event[%d] \"%s\" successfully inserted/updated.", $event["id"],
					$event["title"] ) );
			}
		}

		WP_CLI::success( sprintf( "%d events updated and/or inserted.", $event_inserted ) );
	}
}
WP_CLI::add_command( 'import-event', 'Import_Events_Command' );
