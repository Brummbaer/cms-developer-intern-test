<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cms-developer-intern-test
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) :
			?>
			<div class="entry-meta">
				<?php
				cms_developer_intern_test_posted_on();
				cms_developer_intern_test_posted_by();
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php cms_developer_intern_test_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		$timestamp = get_field('timestamp');

		$timestamp = date_create($timestamp);

		$diff = date_diff(date_create(), $timestamp);
		?>

		<p><strong>About: </strong><?php the_field('about'); ?></p>
		<p><strong>Organized by: </strong> <?php the_field('organizer'); ?></p>
		<p><strong>When: </strong>
			<?php
			// Check and display the event date in the following formats:
			if ($diff->y > 0) {
				printf("In %d years, %d months, %d days, %d hours",
					$diff->y, $diff->m, $diff->d, $diff->h);
			} else if ($diff->m > 0) {
				printf("In %d months, %d days, %d hours",
					$diff->m, $diff->d, $diff->h);
			} else if ($diff->d > 0) {
				printf("In %d days",
					$diff->d);
			} else if ($diff->h > 0) {
				printf("In %d hours, %d minutes, %d seconds",
					$diff->h, $diff->i, $diff->s);
			} else {
				printf("%d minutes, %d seconds",
					$diff->h, $diff->i);
			}
			?>
		</p>
		<p><strong>Organizer email address: </strong><?php the_field('email'); ?></p>
		<p><strong>Address: </strong><?php the_field('address'); ?></p>
		<p><strong>GPS coordinates: </strong><?php the_field('latitude');?>, <?php the_field('longitude'); ?></p>
		<p><?php the_tags(); ?></p>

		<?php
		the_content(
			sprintf(
				wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'cms-developer-intern-test' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'cms-developer-intern-test' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php cms_developer_intern_test_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
