# CMS developer test project

## Getting started

**Want to see the result straight away?**

I hosted the project for you to check it out.

You can go to this link to see all events: http://cms-developer-intern.atwebpages.com/event/

To view the event administration panel, go to this link and login with the credentials provided by email: http://cms-developer-intern.atwebpages.com/wp-admin/edit.php?post_type=event

If you just want to see the relevant code, the custom command lies in the `inc/import-events.php` file.
The CSS is in `style-events.css`, and the custom post type is declared in the `functions.php` file.

## Installation

To use the custom `import-event` command with [*WP-CLI*](https://wp-cli.org/), you must:

- Have a local WordPress installation up and running
- Have the Advanced Custom Fields plugin installed
- Have WP-CLI installed and in your `PATH`

You can then simply run:

`wp theme install https://rblondel.dynv6.net/cms-developer-intern-test/cms-developer-intern-test.zip --activate`

Or download this repository and install the theme manually, by copying it to `wp-content/themes/`.

### Importing the event custom fields

Once the theme is activated, you have to import the custom fields to the ACF plugin.

Go to `YOUR-WORDPRESS-INSTALLATION/wp-admin/edit.php?post_type=acf-field-group&page=acf-tools`, and in the *Import* 
section, choose the `ACF-event-fields.json` file, included in the theme folder or available at this repository's root.

### Using `wp import-event`

You can now use the custom `wp import-event` command:

`wp import-event <JSON_file> [--status=<status>]`

The `status` argument refers to the post's status once it's imported, which can be draft, published, private, or 
pending.
