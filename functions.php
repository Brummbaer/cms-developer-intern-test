<?php
/**
 * cms-developer-intern-test functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package cms-developer-intern-test
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}


/*
* Create our event custom post type
*/
function cms_developer_intern_test_custom_event_post_type() {

// Set UI labels for event post type
	$labels = array(
		'name'                => _x( 'Events', 'Post Type General Name', 'cms-developer-intern-test' ),
		'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'cms-developer-intern-test' ),
		'menu_name'           => __( 'Events', 'cms-developer-intern-test' ),
		'parent_item_colon'   => __( 'Parent event', 'cms-developer-intern-test' ),
		'all_items'           => __( 'All Events', 'cms-developer-intern-test' ),
		'view_item'           => __( 'View Event', 'cms-developer-intern-test' ),
		'add_new_item'        => __( 'Add New Event', 'cms-developer-intern-test' ),
		'add_new'             => __( 'Add New', 'cms-developer-intern-test' ),
		'edit_item'           => __( 'Edit Event', 'cms-developer-intern-test' ),
		'update_item'         => __( 'Update Event', 'cms-developer-intern-test' ),
		'search_items'        => __( 'Search Event', 'cms-developer-intern-test' ),
		'not_found'           => __( 'Not Found', 'cms-developer-intern-test' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'cms-developer-intern-test' ),
	);


	// Set other options for the event post type
	$args = array(
		'label'               => __( 'events', 'cms-developer-intern-test' ),
		'description'         => __( 'event descriptions and details', 'cms-developer-intern-test' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'show_in_rest' => true,
	);

	// Register the event post type
	register_post_type( 'event', $args );
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/
add_action( 'init', 'cms_developer_intern_test_custom_event_post_type', 0 );


if ( ! function_exists( 'cms_developer_intern_test_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function cms_developer_intern_test_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on cms-developer-intern-test, use a find and replace
		 * to change 'cms-developer-intern-test' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'cms-developer-intern-test', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'cms-developer-intern-test' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'cms_developer_intern_test_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);

		if ( defined( 'WP_CLI' ) && WP_CLI ) {
			require_once dirname( __FILE__ ) . '/inc/import-events.php';
		}
	}
endif;
add_action( 'after_setup_theme', 'cms_developer_intern_test_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function cms_developer_intern_test_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'cms_developer_intern_test_content_width', 640 );
}
add_action( 'after_setup_theme', 'cms_developer_intern_test_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function cms_developer_intern_test_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'cms-developer-intern-test' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'cms-developer-intern-test' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'cms_developer_intern_test_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function cms_developer_intern_test_scripts() {
	wp_enqueue_style( 'cms-developer-intern-test-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'cms-developer-intern-test-style-event', get_template_directory_uri() . '/style-events.css');
	wp_style_add_data( 'cms-developer-intern-test-style', 'rtl', 'replace' );

	wp_enqueue_script( 'cms-developer-intern-test-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'cms_developer_intern_test_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}
